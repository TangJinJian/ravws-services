FROM node:12.16.2

WORKDIR /root

COPY . /root

EXPOSE 3000

CMD ["./wait-for-it.sh", "$MONGO_HOST:$MONGO_PORT", "--", "npm", "run", "start:prod"]
