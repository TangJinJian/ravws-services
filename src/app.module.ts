import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TokensModule } from './api/tokens/tokens.module';
import { UsersModule } from './api/users/users.module';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { StaffsModule } from './api/staffs/staffs.module';
import { PasswordModule } from './common/password/password.module';
import { MomentModule } from './common/moment/moment.module';
import { CarwashesModule } from './api/carwashes/carwashes.module';
import { OrdersModule } from './api/orders/orders.module';
import { DashboardModule } from './api/dashboard/dashboard.module';

@Module({
  imports: [
    TokensModule,
    UsersModule,
    MongooseModule.forRoot(new ConfigService().mongodbURI),
    ConfigModule,
    StaffsModule,
    PasswordModule,
    MomentModule,
    CarwashesModule,
    OrdersModule,
    DashboardModule,
  ],
})
export class AppModule {}
