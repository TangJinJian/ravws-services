export enum ErrCode {
  // 请求成功
  Ok = 0,

  // 系统繁忙，此时请开发者稍候再试
  Busy = -1,

  // code 无效
  Invalid = 40029,

  // 频率限制，每个用户每分钟100次
  ApiLimit = 45011,

  // code 已被使用过
  Used = 40163,
}

export interface Code2Session {
  // 会话密钥
  session_key: string;

  // 用户唯一标识
  openid: string;

  // 错误码
  errcode: number;

  // 错误信息
  errmsg: string;
}
