import { Identity } from 'src/schemas/staff/staff.interface';

export interface JwtPayload {
  _id: string;
  identity: Identity;
}
