import { Injectable, HttpService } from '@nestjs/common';
import { ConfigService } from 'src/config/config.service';
import { Code2Session } from './code2session.interface';

@Injectable()
export class TokensService {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  /**
   * code to session
   * @param js_code 微信小程序登录时获取的 code
   */
  code2Session(js_code: string) {
    const {
      code2SessionURL,
      appid,
      secret,
      grant_type,
    } = this.configService.weChat;

    return this.httpService
      .get<Code2Session>(code2SessionURL, {
        params: {
          appid,
          secret,
          js_code,
          grant_type,
        },
      })
      .toPromise();
  }
}
