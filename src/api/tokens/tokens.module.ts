import { Module, HttpModule } from '@nestjs/common';
import { TokensController } from './tokens.controller';
import { TokensService } from './tokens.service';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from 'src/config/config.service';
import { JwtStrategy } from './jwt.strategy';
import { ConfigModule } from 'src/config/config.module';
import { StaffsModule } from '../staffs/staffs.module';
import { PasswordModule } from 'src/common/password/password.module';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    StaffsModule,
    ConfigModule,
    JwtModule.register({
      secret: new ConfigService().jwtSecret,
      signOptions: { expiresIn: '7day' },
    }),
    PasswordModule,
    HttpModule,
    UsersModule,
  ],
  controllers: [TokensController],
  providers: [TokensService, JwtStrategy],
})
export class TokensModule {}
