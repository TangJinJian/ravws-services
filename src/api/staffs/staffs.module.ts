import { Module } from '@nestjs/common';
import { StaffsService } from './staffs.service';
import { MongooseModule } from '@nestjs/mongoose';
import { StaffSchema } from 'src/schemas/staff/staff.schema';
import { StaffsController } from './staffs.controller';
import { PasswordModule } from 'src/common/password/password.module';
import { MomentModule } from 'src/common/moment/moment.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'Staff',
        schema: StaffSchema,
      },
    ]),
    PasswordModule,
    MomentModule,
  ],
  providers: [StaffsService],
  exports: [StaffsService],
  controllers: [StaffsController],
})
export class StaffsModule {}
