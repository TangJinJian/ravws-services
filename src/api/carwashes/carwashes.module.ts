import { Module } from '@nestjs/common';
import { CarwashesController } from './carwashes.controller';
import { CarwashesService } from './carwashes.service';
import { CarwashSchema } from 'src/schemas/carwash/carwash.schema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Carwash', schema: CarwashSchema }]),
  ],
  controllers: [CarwashesController],
  providers: [CarwashesService],
})
export class CarwashesModule {}
