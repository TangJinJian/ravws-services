import { Module } from '@nestjs/common';
import { DashboardController } from './dashboard.controller';
import { DashboardService } from './dashboard.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CarwashSchema } from 'src/schemas/carwash/carwash.schema';
import { OrderSchema } from 'src/schemas/order/order.schema';
import { UserSchema } from 'src/schemas/user/user.schema';
import { MomentModule } from 'src/common/moment/moment.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Carwash', schema: CarwashSchema },
      { name: 'User', schema: UserSchema },
      { name: 'Order', schema: OrderSchema },
    ]),
    MomentModule,
  ],
  controllers: [DashboardController],
  providers: [DashboardService],
})
export class DashboardModule {}
