import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { UserSchema } from 'src/schemas/user/user.schema';
import { MomentModule } from 'src/common/moment/moment.module';
import { VehiclesService } from './vehicles/vehicles.service';
import { VehiclesController } from './vehicles/vehicles.controller';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    MomentModule,
  ],
  controllers: [UsersController, VehiclesController],
  providers: [UsersService, VehiclesService],
  exports: [UsersService],
})
export class UsersModule {}
