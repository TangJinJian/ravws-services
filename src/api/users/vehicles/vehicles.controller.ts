import {
  Controller,
  Patch,
  UseGuards,
  Param,
  Body,
  Put,
  Delete,
  Get,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import { VehicleDto } from 'src/dto/vehicle.dto';
import { VehiclesService } from './vehicles.service';

@UseGuards(JwtAuthGuard)
@Controller('users/:_id/vehicles')
export class VehiclesController {
  constructor(private readonly vehiclesService: VehiclesService) {}

  /**
   * 创建一个车辆
   * @param openid 用户唯一标识
   * @param vehicleDto 车辆信息
   */
  @Patch()
  async createVehicle(
    @Param('_id') _id: string,
    @Body() vehicleDto: VehicleDto,
  ) {
    await this.vehiclesService.createById(_id, vehicleDto);
  }

  /**
   * 创建一个车辆
   * @param openid 用户唯一标识
   * @param vehicleDto 车辆信息
   */
  @Put()
  async createVehicleWeChat(
    @Param('openid') openid: string,
    @Body() vehicleDto: VehicleDto,
  ) {
    await this.createVehicle(openid, vehicleDto);
  }

  /**
   * 删除一个车辆
   * @param _id 用户 _id
   * @param __id 车辆 _id
   */
  @Delete(':__id')
  async deleteVehicle(
    @Param('_id') _id: string,
    @Param('__id') __id: string,
  ) {
    await this.vehiclesService.deleteById(_id, __id);
  }

  /**
   * 查询一些车辆
   * @param _id 用户 _id
   */
  @Get()
  async findVehicles(@Param('_id') _id: string) {
    return await this.vehiclesService.findVehiclesById(_id);
  }
}
