import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from 'src/schemas/user/user.interface';
import { VehicleDto } from 'src/dto/vehicle.dto';

@Injectable()
export class VehiclesService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  /**
   * 创建一个车辆
   * @param _id 用户 _id
   * @param vehicleDto 车辆信息
   */
  createById(_id: string, vehicleDto: VehicleDto) {
    return this.userModel.updateOne(
      { _id, 'vehicles.plateNumber': { $ne: vehicleDto.plateNumber } },
      { $push: { vehicles: vehicleDto } },
    );
  }

  /**
   * 删除一个车辆
   * @param _id 用户 _id
   * @param __id 车辆 _id
   */
  deleteById(_id: string, __id: string) {
    return this.userModel.updateOne(
      { _id },
      {
        $pull: {
          vehicles: {
            _id: __id,
          },
        },
      },
    );
  }

  /**
   * 查询一些车辆
   * @param _id 用户 _id
   */
  async findVehiclesById(_id: string) {
    return (await this.userModel.findOne({ _id })).vehicles;
  }
}
