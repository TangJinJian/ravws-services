/* eslint-disable @typescript-eslint/no-inferrable-types */
import {
  Controller,
  UseGuards,
  Param,
  Delete,
  Patch,
  Body,
  Put,
  Get,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  Query,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import { UsersService } from './users.service';
import { UpdateUserDto } from 'src/dto/update-user.dto';
import { FindDataDto } from 'src/dto/find-data.dto';
import { UserDto } from 'src/dto/user.dto';

@UseGuards(JwtAuthGuard)
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  /**
   * 删除一个用户
   * @param openid 用户唯一标识
   */
  @Delete(':_id')
  async deleteUser(@Param('_id') _id: string) {
    await this.usersService.deleteById(_id);
  }

  /**
   * 更新一个用户
   * @param openid 用户唯一标识
   * @param updateUserDto 更新的用户信息
   */
  @Patch(':_id')
  async updateUser(
    @Param('_id') _id: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    await this.usersService.updateById(_id, updateUserDto);
  }

  /**
   * 更新一个用户
   * @param openid 用户唯一标识
   * @param updateUserDto 更新的用户信息
   */
  @Put(':_id')
  async updateUserWeChat(
    @Param('_id') _id: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    await this.updateUser(_id, updateUserDto);
  }

  /**
   * 查询一些用户
   * @param pageIndex 页码
   * @param pageSize 页大小
   */
  @Get()
  async findUsers(
    @Query('pageIndex') pageIndex: number = 1,
    @Query('pageSize') pageSize: number = 10,
    @Query('keyword') keyword: string = '',
  ) {
    const data = await this.usersService.find({
      pageIndex,
      pageSize,
      keyword,
    });
    const total = await this.usersService.count({ keyword });

    return {
      total,
      pageIndex,
      pageSize,
      keyword,
      data,
    } as FindDataDto<UserDto>;
  }
}
