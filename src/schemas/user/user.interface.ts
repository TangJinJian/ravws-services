import { Document } from 'mongoose';

export interface Vehicle extends Document {
  // 车牌号
  readonly plateNumber: string;

  // 车型
  readonly type: number;
}

export interface User extends Document {
  // 用户唯一标识
  readonly openid: string;

  // 昵称
  readonly nickName: string;

  // 手机号码
  readonly phoneNumber: string;

  // 车辆
  readonly vehicles: [Vehicle];

  // 创建时间
  readonly creationTime: Date;
}
