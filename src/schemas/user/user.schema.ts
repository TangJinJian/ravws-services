import * as mongoose from 'mongoose';

export const VehicleSchema = new mongoose.Schema({
  // 车牌号
  plateNumber: { type: String, unique: true },

  // 车型
  type: { type: Number, min: 0 },
});

export const UserSchema = new mongoose.Schema({
  // 用户唯一标识
  openid: { type: String, unique: true },

  // 昵称
  nickName: String,

  // 手机号码
  phoneNumber: String,

  // 车辆
  vehicles: [VehicleSchema],

  // 创建时间
  creationTime: { type: Date, default: Date.now() + 28800000 },
});
