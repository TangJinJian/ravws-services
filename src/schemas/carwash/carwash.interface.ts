import { Document } from 'mongoose';
import { CarwashState } from 'src/dto/carwash.dto';

export interface Carwash extends Document {
  // 名称
  readonly name: string;

  // 城市
  readonly city: string[];

  // 地址
  readonly address: string;

  // 经度
  readonly longitude: string;

  // 纬度
  readonly latitude: string;

  // 价格
  readonly price: number;

  // 状态
  readonly state: CarwashState;
}
