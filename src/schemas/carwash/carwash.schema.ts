import * as mongoose from 'mongoose';

export const CarwashSchema = new mongoose.Schema({
  // 名称
  name: String,

  // 城市
  city: [String],

  // 地址
  address: String,

  // 经度
  longitude: String,

  // 纬度
  latitude: String,

  // 价格
  price: { type: Number, min: 0 },

  // 状态
  state: { type: Number, min: 0, max: 2 },
});
