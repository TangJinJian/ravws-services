import * as mongoose from 'mongoose';

export const StaffSchema = new mongoose.Schema({
  // 账号
  account: { type: String, unique: true },

  // 密码
  password: String,

  // 身份 0：管理员、1：普通员工、2：微信小程序用户
  identity: { type: Number, min: 0, max: 2, default: 0 },
});
