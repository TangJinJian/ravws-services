import { Document } from 'mongoose';

// 身份枚举
export enum Identity {
  // 员工管理员
  StaffAdmin,

  // 员工
  Staff,

  // 微信用户
  WeChat,
}

export interface Staff extends Document {
  // 账号
  readonly account: string;

  // 密码
  readonly password: string;

  // 身份
  readonly identity: Identity;
}
