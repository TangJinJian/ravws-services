import * as mongoose from 'mongoose';

export const OrderSchema = new mongoose.Schema({
  // 订单号
  // _id

  // 预约时间
  appointmentTime: String,

  // 用户 _id
  user: mongoose.SchemaTypes.ObjectId,

  // 用户昵称
  nickName: String,

  // 订单状态。0：已创建、1：已支付、2：已关闭
  state: { type: Number, min: 0, max: 2, default: 0 },

  // 支付金额
  amount: Number,

  // 洗车点 _id
  carwash: mongoose.SchemaTypes.ObjectId,

  // 洗车点名称
  name: String,

  // 创建时间
  creationTime: { type: Date, default: Date.now() + 28800000 },
});
