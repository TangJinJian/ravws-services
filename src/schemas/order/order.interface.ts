import { Document } from 'mongoose';
import { OrderState } from 'src/dto/order.dto';

export interface Order extends Document {
  // 订单号
  // _id

  // 预约时间
  readonly appointmentTime: string;

  // 用户
  readonly user: string;

  // 用户昵称
  readonly nickName: string,

  // 订单状态。0：已创建、1：已支付、2：已关闭
  readonly state: OrderState;

  // 支付金额
  readonly amount: number;

  // 洗车点
  readonly carwash: string;

  // 洗车点名称
  readonly name: string,

  // 创建时间
  readonly creationTime: Date;
}
