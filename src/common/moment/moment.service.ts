import { Injectable } from '@nestjs/common';
import * as moment from 'moment';

@Injectable()
export class MomentService {
  get creationTime() {
    return moment()
      .utcOffset(480)
      .format('YYYY-MM-DD HH:mm:ss');
  }

  /**
   * 返回仅有日期，时分秒归零的日期对象
   */
  get utcAdd8Date() {
    const date = moment()
      .utcOffset(480)
      .format('YYYY-MM-DD');
    return new Date(date);
  }
}
