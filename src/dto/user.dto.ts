import { VehicleDto } from './vehicle.dto';

export class UserDto {
  _id?: string;

  // 用户唯一标识
  openid: string;

  // 昵称
  nickName: string;

  // 手机号码
  phoneNumber: string;

  // 一些车辆
  vehicles: VehicleDto[];
}
