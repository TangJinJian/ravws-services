import { IsString, IsArray } from 'class-validator';
import { VehicleDto } from './vehicle.dto';

export class UpdateUserDto {
  // 昵称
  @IsString()
  nickName: string;

  // 手机号码
  @IsString()
  phoneNumber: string;

  // 车辆
  @IsArray()
  vehicles: [VehicleDto];
}
