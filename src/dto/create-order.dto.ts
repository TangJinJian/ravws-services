export class CreateOrderDto {
  // 用户昵称
  nickName: string;

  // 预约时间
  appointmentTime: string;

  // 支付金额
  amount: number;

  // 洗车点 _id
  carwash: string;

  // 洗车点名称
  name: string;
}
