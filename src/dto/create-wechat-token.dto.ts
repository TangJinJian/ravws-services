import { IsString, IsNotEmpty } from 'class-validator';

export class CreateWeChatTokenDto {
  // 账号
  @IsString({ message: '您填写的不是一个文本' })
  @IsNotEmpty({ message: '您没有填写 code' })
  js_code: string;

  // 密码
  @IsString({ message: '您填写的不是一个文本' })
  @IsNotEmpty({ message: '您没有填写昵称' })
  nickName: string;
}
