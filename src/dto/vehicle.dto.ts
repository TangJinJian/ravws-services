import { IsInt, IsString } from 'class-validator';

export class VehicleDto {
  // 车牌号
  @IsString()
  plateNumber: string;

  // 车型
  @IsInt()
  type: number;
}
