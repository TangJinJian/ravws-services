export class StatisticDto {
  // 累计洗车点
  cumulativeCarwashes: number;

  // 累计用户
  cumulativeUsers: number;

  // 今日新增用户
  todayNewUsers: number;

  // 累计订单
  cumulativeOrders: number;

  // 今日新增订单
  todayNewOrders: number;

  // 累计营业额
  cumulativeTurnover: number;

  // 今日营业额
  todayNewTurnover: number;
}
