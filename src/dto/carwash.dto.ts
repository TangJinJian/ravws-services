// 订单状态
export enum CarwashState {
  // 空闲中
  InFree,

  // 洗车中
  WashingCar,

  // 维护中
  UnderMaintenance,
}

export class CarwashDto {
  _id?: string;

  // 名称
  name: string;

  // 城市
  city: string[];

  // 地址
  address: string;

  // 经度
  longitude: string;

  // 纬度
  latitude: string;

  // 价格
  price: number;

  // 状态
  state: CarwashState;
}
