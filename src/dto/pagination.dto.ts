import { Min, IsInt } from 'class-validator';

export class PaginationDto {
  // 页码
  @IsInt({ message: '页码需要为整数' })
  @Min(1)
  pageIndex?: number;

  // 页大小
  @IsInt({ message: '页大小需要为整数' })
  @Min(1)
  pageSize?: number;

  // 关键字
  keyword?: string;
}
