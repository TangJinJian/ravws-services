// 订单状态
export enum OrderState {
  // 已创建
  Created,

  // 已支付
  HavePay,

  // 已完成
  Completed,

  // 已关闭
  Closed,
}

export class OrderDto {
  _id?: string;

  // 状态
  state: OrderState;

  // 预约时间
  appointmentTime: string;

  // 用户 _id
  user: string;

  // 用户昵称
  nickName: string;

  // 支付金额
  amount: number;

  // 洗车点 _id
  carwash: string;

  // 洗车点名称
  name: string;
}
