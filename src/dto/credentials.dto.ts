import { IsString, IsNotEmpty } from 'class-validator';

export class CredentialsDto {
  // 账号
  @IsString({ message: '您填写的不是一个文本' })
  @IsNotEmpty({ message: '您没有填写账号' })
  account: string;

  // 密码
  @IsString({ message: '您填写的不是一个文本' })
  @IsNotEmpty({ message: '您没有填写密码' })
  password: string;
}
