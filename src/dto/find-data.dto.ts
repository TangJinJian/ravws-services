import { PaginationDto } from './pagination.dto';

export class FindDataDto<T> extends PaginationDto {
  // 总计
  total: number;

  // 一些数据
  data: T[];
}
