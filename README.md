# `ravws-services`

#### 一、介绍
远程自动洗车系统-服务端

#### 二、部署

```bash
# 克隆代码
git clone https://gitee.com/TangJinJian/ravws-services.git

# 打开项目目录
cd ravws-services

# 创建环境目录
mkdir src/env

# 创建环境文件，参照“三、环境文件配置”
touch src/env/config.dev.env.ts
touch src/env/config.prod.env.ts

# 安装依赖。因为安装 bcrypt 需要权限，所以使用 sudo
sudo npm i

# 热重载开发
npm run start:dev

# 构建
npm run build

# 运行
npm run start:prod
```

#### 三、环境文件配置

```js
/* eslint-disable @typescript-eslint/camelcase */
module.exports = {
  // jwt 密钥
  jwtSecret: 'jwtSecret',

  // MongoDB 配置
  mongo: {
    host: 'localhost',
    port: '27017',
    db: 'ravws',
    // initdb: {
    //   username: 'username',
    //   password: 'password',
    // },
  },

  // 微信小程序配置
  weChat: {
    // 小程序 appId
    appid: '<appid>',
    // 小程序 appSecret
    secret: '<secret>',
    // 授权类型，此处只需填写 authorization_code
    grant_type: 'authorization_code',
    // 小程序登录凭证校验请求地址
    code2SessionURL: 'https://api.weixin.qq.com/sns/jscode2session',
  },
};
```

